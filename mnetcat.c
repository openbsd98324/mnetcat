

/// ======================
///      Mini Netcat 
/// ======================

// cc mininetcat.c  -o mininetcat 
/// It sends the filename, content, and it returns the link (url) (for termbin.com). 


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>

#define MAX_LINE 4096
#define BUFFSIZE 4096




void sendfile(  FILE *fp, int sockfd);
void writefile( int sockfd, FILE *fp);
ssize_t total=0;



int main(int argc, char* argv[])
{

    if ( argc <= 3 ) 
    {
        printf( " Usage: mnetcat >file.txt> <IPaddress> <Port> \n");
        printf( "\n" ); 
        printf( "    mnetcat  /tmp/mping.c    5.39.93.71   9999 \n"); 
        printf( "    It returns: https://termbin.com/smdd \n" ); 
        printf( "\n" ); 
        exit(1);
    }

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
    {
        perror("Can't allocate sockfd");
        exit(1);
    }

    struct sockaddr_in serveraddr;

    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons( atoi( argv[ 3 ] ) );

    if (inet_pton(AF_INET, argv[2], &serveraddr.sin_addr) < 0)
    {
        perror("IPaddress Convert Error");
        exit(1);
    }


    if ( connect(sockfd, (const struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0)
    {
        perror("Connect Error");
        exit(1);
    }
    
    char *filename = basename( argv[1] ); 
    if (filename == NULL)
    {
        perror("Can't get filename");
        exit(1);
    }
    
    char buff[BUFFSIZE] = {0};
    strncpy(buff, filename, strlen(filename));
    //if ( send(sockfd, buff, BUFFSIZE , 0) == -1)
    if ( send(sockfd, buff, strlen( filename) , 0) == -1)
    {
        perror( "Can't send filename");
        exit(1);
    }
    
    printf( "FILE: %s\n", argv[ 1 ] ); 
    FILE *fp = fopen( argv[1], "rb");
    if (fp == NULL) 
    {
        perror("Can't open file");
        exit(1);
    }

    sendfile( fp, sockfd );
    printf("Send Success, NumBytes = %ld\n", total);

    char addr[INET_ADDRSTRLEN];
    printf("Start receive file: %s from %s\n", filename, inet_ntop(AF_INET, &serveraddr.sin_addr, addr, INET_ADDRSTRLEN));
    writefile( sockfd, fp );
    printf("Receive Success, NumBytes = %ld\n", total);

    fclose(fp);
    close(sockfd);
    return 0;
}











void sendfile( FILE *fp, int sockfd ) 
{
    int n; 
    char sendline[MAX_LINE] = {0}; 

    while (( n = fread( sendline, sizeof(char), MAX_LINE, fp)) > 0) 
    {
	total+=n;
        printf( "Line %s\n" , sendline ); 
        if (n != MAX_LINE && ferror(fp))
        {
            perror("Read File Error");
            exit(1);
        }
        
        if ( send( sockfd, sendline, n, 0) == -1)
        {
            perror("Can't send file");
            exit(1);
        }
        memset( sendline, 0, MAX_LINE );
    }
}






void writefile(int sockfd, FILE *fp)
{
    ssize_t n;
    char buff[MAX_LINE] = {0};
    while (( n = recv( sockfd, buff, MAX_LINE, 0)) > 0) 
    {
	total+=n;
        if (n == -1)
        {
            perror("Receive File Error");
            exit(1);
        }
        
	printf( "%s\n" , buff ); 
        memset(buff, 0, MAX_LINE);
    }
}




